# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20140926_2347'),
    ]

    operations = [
        migrations.CreateModel(
            name='YouTube',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=40, null=True, verbose_name='t\xedtulo', blank=True)),
                ('video_id', models.CharField(max_length=60, verbose_name='video id')),
                ('autoplay', models.BooleanField(default=False, verbose_name='autoplay')),
                ('allow_fullscreen', models.BooleanField(default=True, verbose_name='allow fullscreen')),
                ('loop', models.BooleanField(default=False, verbose_name='loop')),
                ('display_related_videos', models.BooleanField(default=False, verbose_name='display related videos')),
                ('high_quality', models.BooleanField(default=False, verbose_name='high quality')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
