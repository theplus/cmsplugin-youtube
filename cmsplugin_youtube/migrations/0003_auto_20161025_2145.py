# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsplugin_youtube', '0002_auto_20161025_2038'),
    ]

    operations = [
        migrations.RunSQL("ALTER TABLE cmsplugin_youtube RENAME TO cmsplugin_youtube_youtube")
    ]
