# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsplugin_youtube', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='youtube',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='cmsplugin_youtube_youtube', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
    ]
